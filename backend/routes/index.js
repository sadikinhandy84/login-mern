import express from "express";
import { refreshToken } from "../controllers/RefreshToken.js";
import { getUsers, Register, Login, logout } from "../controllers/Users.js"; //import controller
import { verifyToken } from "../middleware/verifyToken.js"; // import middleware token


const router  = express.Router();


router.get('/users', verifyToken, getUsers);
router.post('/users',Register);
router.post('/login',Login);
router.get('/token',refreshToken);
router.get('/logout',logout);

export default router;


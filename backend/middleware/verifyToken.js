import jwt from "jsonwebtoken";

export const verifyToken = (req, res, next) => {
    //authorization
    const authHeader = req.headers.authorization;
     //ambil token yang dikirim dari client
    const token  = authHeader && authHeader.split(" ")[1];

    if (token == null) return res.sendStatus(401); //jikan token kosong
    //varifikasi token jika ada
    jwt.verify(token,process.env.ACCESS_TOKEN_SECRET,(err,decoded) => {
        //jikan error
        if(err) return res.sendStatus(403);
        //jika berhasil
        req.email = decoded.email;
        next();
    })


    
    
}
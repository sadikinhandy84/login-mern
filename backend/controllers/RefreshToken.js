import Users from "../models/UserModel.js"; //import user model
import jwt from "jsonwebtoken"; //import jwt

//funngsi refresh token
export const refreshToken  = async(req,res) => {
    try {
        //ambil token pada cookies
        const refreshToken = req.cookies.refreshToken;
        //validasi hasil cookies
        
        if(!refreshToken) return res.sendStatus(401);//jika token kosong makan status unauthorized
         //jika ada kita bandingkan token yang dikirim oleh client dengan token
         //yang ada pada cookies 
         const user = await Users.findAll({
            where: {
                refresh_token: refreshToken
            }
         });
         if(!user[0]) return res.send(user[0]); //jika data tidak cocok maka kirim status forbidden
         jwt.verify(refreshToken,process.env.REFRESH_TOKEN_SECRET,(err,decoded) => {
            if(err) return res.sendStatus(403); // jika error forbidden 
            //jika berhasil
            const userId = user[0].id;
            const name = user[0].name;
            const email = user[0].email;
            //buat refresh token
            const accessToken = jwt.sign({userId, name, email}, process.env.ACCESS_TOKEN_SECRET,{
                expiresIn: '15s'
            });
            res.json({ accessToken });
         });
        } catch (error) { //jika error
        console.log(error);
    }
}
